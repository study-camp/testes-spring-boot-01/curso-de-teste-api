package cap.wesantos.cursotestesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoTestesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CursoTestesApiApplication.class, args);
    }

}
